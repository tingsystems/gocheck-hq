import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

// Routes
import { AppRoutingModule } from './app-routing.module';

// Services
import { AuthService } from './services/auth/auth.service';
import { GeoService } from './services/geo/geo.service';

// components
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { HomeComponent } from './components/home/home.component';
import { CheckinComponent } from './components/checkin/checkin.component';
import { LoginComponent } from './components/login/login.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    CheckinComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [ 
    AuthService, 
    GeoService,
    { provide: 'apiBaseUrl', useValue: 'http://192.168.1.66:3000/api'}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
