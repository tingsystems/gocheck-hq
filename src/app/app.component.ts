import { Component } from '@angular/core';

import { AuthService } from './services/auth/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {

  isLogged: boolean;

  constructor( private authSrv: AuthService) {
    this.isLogged = this.authSrv.isLogged();
  }

  title = 'gocheck-hq';
}
