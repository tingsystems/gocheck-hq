export class UserModel {
    id: string;
    firstName: string;
    lastName: string;
}

export class LoginModel {
    username: string;
    password: string;
}