import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';


export interface User {
  id: number;
  username: string;
  firstName: string;
  isAdmin: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  users: User[] = [
    {id: 1, username: 'cirilo', firstName: 'Cirilo', isAdmin: true},
    {id: 2, username: 'frida', firstName: 'Frida', isAdmin: false},
    {id: 3, username: 'bibi', firstName: 'Bibi', isAdmin: false},
  ]

  access: string;

  constructor( @Inject('apiBaseUrl') private apiBaseUrl: string, private httpSrv: HttpClient ) { 
    this.getAccessToken();
  }

  login(loginData: Object) {
    return this.httpSrv.post(`${this.apiBaseUrl}/login/`, loginData)
              .pipe( map( resp => {
                  this.saveToken(resp['access']);
                  return resp;
              }));
  }

  logout() {
    localStorage.removeItem('token');
  }

  private saveToken( token: string ) {
    this.access = token;
    localStorage.setItem('token', token);
  }

  getAccessToken() {
    if ( localStorage.getItem('token') ) {
      this.access = localStorage.getItem('token');
    } else {
      this.access = '';
    }
  }

  isLogged(): boolean {
    if ( this.access.length < 2) {
      return false;
    }
    return true;
  }

  getUsers() {
    return this.users;
  }

}
