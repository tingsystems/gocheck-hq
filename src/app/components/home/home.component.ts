import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService, User } from "../../services/auth/auth.service";
import { GeoService } from "../../services/geo/geo.service";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass']
})
export class HomeComponent implements OnInit {

  users: User[] = [];
  countries: any[] = [];
  messageError: string;

  constructor( private router: Router, private authSrv: AuthService, private geoSrv: GeoService) {
    this.geoSrv.getCountries().subscribe( (countries: any) => {
      this.countries = countries;
    }, (error) => {
      this.messageError = error;
    });
  }

  ngOnInit() {
    this.users = this.authSrv.getUsers();
  }

  goTo(routeName, param) {
    this.router.navigate( [routeName, param] );  
  }

}
