import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-checkin',
  templateUrl: './checkin.component.html',
  styleUrls: ['./checkin.component.sass']
})
export class CheckinComponent implements OnInit {

  idUser: string = '';

  constructor( private activateRoute: ActivatedRoute) {
    this.activateRoute.params.subscribe(params => {
      this.idUser = params['id'];
      console.log(params['id']);
    });
  }

  ngOnInit() {
  }

}
