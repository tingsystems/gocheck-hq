import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

import { LoginModel } from '../../models/user.models';
import { AuthService } from '../../services/auth/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {

  loginData: LoginModel;

  constructor( private authSrv: AuthService, private router: Router ) { 
    this.loginData = new LoginModel();
  }

  ngOnInit() {
  }

  onSubmitLogin( form: NgForm) {
    if ( form.invalid ) { return; }
    this.authSrv.login(this.loginData).subscribe( (data) => {
      this.router.navigateByUrl('/home');
    }, (error) => {
      console.log(error);
    });
  }

}
